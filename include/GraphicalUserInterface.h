/*
    GraphicalUserInterface for display

    2016-08-22 Lin Zhang
    The Hamlyn Centre for Robotic Surgery,
    Imperial College, London
*/

#ifndef GraphicalUserInterface_H
#define GraphicalUserInterface_H

#include "ui_GraphicalUserInterface.h"

#include <QMainWindow>
/*#include <opencv2/opencv.hpp>*/

#include <mutex>


class GraphicalUserInterface : public QMainWindow, private Ui::GraphicalUserInterface
{
    Q_OBJECT
public:

    GraphicalUserInterface();

Q_SIGNALS:

	void LoadCam();

    /***
     * Record related
     * */
    void StartRecord(QString save_dir);
    void StopRecord();

public slots:

    virtual void slotExit();

    void newCameraImage(QImage);

private slots:

	void on_pushButton_loadCam_released();

    void setInfo1(QString message);

    void setInfo2(QString message);

    void on_pushButton_StartRecord_released();

    void on_pushButton_StopRecord_released();

private:

};


#endif
