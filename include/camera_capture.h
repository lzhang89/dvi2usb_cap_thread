/*
    Camera Capture class run in different thread

	2016-08-22 Lin Zhang
	The Hamlyn Centre for Robotic Surgery,
	Imperial College, London
*/


#ifndef CAMERA_CAPTURE_H
#define CAMERA_CAPTURE_H
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <QThread>
#include <QTimer>
#include <QImage>

class CameraCapture : public QObject
{
	Q_OBJECT
public:
    CameraCapture ();
	
	// Start camera capture
	void start();

	// --- Helper function ---
	QImage cvtCvMat2QImage(const cv::Mat & image);
	cv::Mat cvtQImage2CvMat( const QImage &inImage, bool inCloneImageData = true );

Q_SIGNALS:

	void newCameraImage(QImage);
	void setGUIInfo1(QString);
	void setGUIInfo2(QString);
	void stopTimer();

private slots:
	void process();
	void StartRecord(QString);
	void StopRecord();
	void OpenCam();
	void aboutToQuit();

protected:

	QTimer mTimer_;
	QThread mThread_;

private:
	// VideoCapture
	cv::Ptr<cv::VideoCapture> cam_cap;

	// VideoWriter
	cv::Ptr<cv::VideoWriter> cam_writer;

	cv::Mat curr_img;
	cv::Size cam_img_size;

	bool is_record_data;
};

#endif  // CAMERA_CAPTURE_H
