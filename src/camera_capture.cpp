#include "camera_capture.h"
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>

#include <QDebug>

#include <time.h>
#include <iostream>

CameraCapture::CameraCapture ()
{
	cam_cap = new cv::VideoCapture();
	cam_writer = new cv::VideoWriter();

	// Connect timer for servo loop
	QObject::connect(&mTimer_, &QTimer::timeout, this, &CameraCapture::process);
	QObject::connect(this, &CameraCapture::stopTimer, &mTimer_, &QTimer::stop);
	
}

void CameraCapture::start()
{
	mTimer_.start(40);	// Process runs at every 40ms (25Hz)
	mTimer_.moveToThread(&mThread_);    // Run timer in different thread
	this->moveToThread(&mThread_);
	mThread_.start();
}

void CameraCapture::process()
{
	if (cam_cap->isOpened())
	{
		cam_cap->read(curr_img);

		if (is_record_data)
		{
			cam_writer->write(curr_img);
		}
		QImage qtemp = cvtCvMat2QImage(curr_img);
		Q_EMIT newCameraImage(qtemp);
	}
}

void CameraCapture::StartRecord(QString save_dir)
{
	// Make sure VideoWriter is free to use
	if (cam_writer->isOpened())
		cam_writer->release();

	std::string cam_vid_filename;

	time_t t = time(0);   // get time now
	struct tm * now = localtime( & t );

	// #ifdef _DEBUG
	// 
	// #else
	char buffer_cam_vid [100];
	strftime (buffer_cam_vid,80,"ARIUS-Camera-%Y%b%d%H%M%S",now);
	cam_vid_filename = save_dir.toLatin1();
	cam_vid_filename += buffer_cam_vid;
	cam_vid_filename += ".avi";

	// #endif


	// Remember to have even number of width and height for video compatibility
	if (cam_cap->isOpened())
	{
		cam_writer->open(cam_vid_filename, CV_FOURCC('X','V','I','D'), 25, cam_img_size);
		std::cout << "Writing Camera video to: " << cam_vid_filename << std::endl;
	}

	is_record_data = true;

	Q_EMIT setGUIInfo2("Recording...");
}

void CameraCapture::StopRecord()
{
	if (is_record_data)
	{
		is_record_data = false;
		cam_writer->release();
		Q_EMIT setGUIInfo2("Record is STOP");
	}

}

void CameraCapture::OpenCam()
{
	if(cam_cap->isOpened())
	{
		cam_cap->release();
	}
	cam_cap->open(0);

	if(!cam_cap->isOpened())
	{
		std::cout << "Read camera error" << std::endl;
	}

	cam_cap->read(curr_img); 
	cam_img_size = curr_img.size();
}

void CameraCapture::aboutToQuit()
{
	Q_EMIT stopTimer();
}


QImage  CameraCapture::cvtCvMat2QImage( const cv::Mat &inMat )
{
	switch ( inMat.type() )
	{
		// 8-bit, 4 channel
	case CV_8UC4:
		{
			QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );

			return image;
		}

		// 8-bit, 3 channel
	case CV_8UC3:
		{
			QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );

			return image.rgbSwapped();
		}

		// 8-bit, 1 channel
	case CV_8UC1:
		{
			static QVector<QRgb>  sColorTable;

			// only create our color table once
			if ( sColorTable.isEmpty() )
			{
				for ( int i = 0; i < 256; ++i )
					sColorTable.push_back( qRgb( i, i, i ) );
			}

			QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );

			image.setColorTable( sColorTable );

			return image;
		}

	default:
		qWarning() << "ASM::cvMatToQImage() - cv::Mat image type not handled in switch:" << inMat.type();
		break;
	}

	return QImage();
}


// If inImage exists for the lifetime of the resulting cv::Mat, pass false to inCloneImageData to share inImage's
// data with the cv::Mat directly
//    NOTE: Format_RGB888 is an exception since we need to use a local QImage and thus must clone the data regardless
cv::Mat CameraCapture::cvtQImage2CvMat( const QImage &inImage, bool inCloneImageData )
{
	switch ( inImage.format() )
	{
		// 8-bit, 4 channel
	case QImage::Format_RGB32:
		{
			cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC4, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

			return (inCloneImageData ? mat.clone() : mat);
		}

		// 8-bit, 3 channel
	case QImage::Format_RGB888:
		{
			if ( !inCloneImageData )
				qWarning() << "ASM::QImageToCvMat() - Conversion requires cloning since we use a temporary QImage";

			QImage   swapped = inImage.rgbSwapped();

			return cv::Mat( swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() ).clone();
		}

		// 8-bit, 1 channel
	case QImage::Format_Indexed8:
		{
			cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC1, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

			return (inCloneImageData ? mat.clone() : mat);
		}

	default:
		qWarning() << "ASM::QImageToCvMat() - QImage format not handled in switch:" << inImage.format();
		break;
	}

	return cv::Mat();
}