#include <QApplication>

#include "GraphicalUserInterface.h"
#include "camera_capture.h"


///Connect Signals and Slots only relevant for the graphical interface
void gui_connections(GraphicalUserInterface* gui
                     , CameraCapture* cam_cap)
{
	QObject::connect(gui, SIGNAL(StartRecord(QString)), cam_cap, SLOT(StartRecord(QString)));
	QObject::connect(gui, SIGNAL(StopRecord()), cam_cap, SLOT(StopRecord()));
	QObject::connect(gui, SIGNAL(LoadCam()), cam_cap, SLOT(OpenCam()));

 
	QObject::connect(cam_cap,  SIGNAL(newCameraImage(QImage)), gui, SLOT(newCameraImage(QImage)));
	QObject::connect(cam_cap,  SIGNAL(setGUIInfo1(QString)), gui, SLOT(setInfo1(QString)));
	QObject::connect(cam_cap,  SIGNAL(setGUIInfo2(QString)), gui, SLOT(setInfo2(QString)));
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("DVI2USB Camera Capture");

	CameraCapture cap;

    GraphicalUserInterface *gui = new GraphicalUserInterface();
    gui->show();

    gui_connections(gui, &cap);

	QObject::connect(&app, SIGNAL(aboutToQuit()), &cap, SLOT(aboutToQuit()));

    cap.start();
    return app.exec();
}
