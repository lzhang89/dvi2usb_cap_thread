#include "GraphicalUserInterface.h"
#include <QPainter>
#include <QPen>
#include <QFileDialog>
#include <iostream>


// Constructor
GraphicalUserInterface::GraphicalUserInterface()
{
	this->setupUi(this);

	// Set up action signals and slots
	connect(this->actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));
}


void GraphicalUserInterface::slotExit()
{
	qApp->exit();
}


void GraphicalUserInterface::newCameraImage(QImage qimage){

	if (tabWidget->currentIndex() == 0)
	{
		if(camera_label->isVisible()){
			//         camera_label->setPixmap(QPixmap::fromImage(qimage).scaled(720,720,Qt::KeepAspectRatio));
			camera_label->setPixmap(QPixmap::fromImage(qimage));
			camera_label->repaint();
		}
	}
}

void GraphicalUserInterface::on_pushButton_loadCam_released()
{
	Q_EMIT LoadCam();
}

void GraphicalUserInterface::setInfo1(QString message)
{
	info_label1->setText(message);
	info_label1->repaint();
}

void GraphicalUserInterface::setInfo2(QString message)
{
	info_label2->setText(message);
	info_label2->repaint();
}


void GraphicalUserInterface::on_pushButton_StartRecord_released()
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
		"/home",
		QFileDialog::ShowDirsOnly
		| QFileDialog::DontResolveSymlinks);
	if (dir.isNull())
		return;
	Q_EMIT StartRecord(dir);
}

void GraphicalUserInterface::on_pushButton_StopRecord_released()
{
	Q_EMIT StopRecord();
}